#!/usr/bin/env python

from __future__ import print_function

import optparse
import os
import json
import sys
import urllib

import requests
import toml


PUSHOVER_API_URL = 'https://api.pushover.net/1/messages.json'

DEFAULT_STR = 'default'

NOTIFICATION_TEXT = {
    'PROBLEM':           'Problem',
    'RECOVERY':          'Recovery',
    'ACKNOWLEDGEMENT':   'Ack',
    'FLAPPINGSTART':     'Flap Start',
    'FLAPPINGSTOP':      'Flap Stop',
    'FLAPPINGDISABLED':  'Flap Disable',
    'DOWNTIMESTART':     'Down Start',
    'DOWNTIMEEND':       'Down End',
    'DOWNTIMECANCELLED': 'Down Cancel',
    'CUSTOM':            'Custom',
}

NOTIFICATION_STATUSES = {
    'FLAPPINGSTART':     'Service has begun flapping (notifications disabled).',
    'FLAPPINGSTOP':      'Service has stopped flapping (notifications enabled).',
    'FLAPPINGDISABLED':  'Flap detection has been turned off (notifications enabled).',
    'DOWNTIMESTART':     'Now in scheduled downtime (notifications disabled).',
    'DOWNTIMEEND':       'Scheduled downtime finished (notifications enabled).',
    'DOWNTIMECANCELLED': 'Scheduled downtime cancelled early (notifications enabled).',
    'CUSTOM':            '',
}


def check_option(name, value):
    if value is None:
        print('No {} given.'.format(name), file=sys.stderr)
        exit(1)
        
def add_param(payload, key, value, configuration=None):
    if value is not None:
        payload[key] = value
    elif configuration is not None and key in configuration:
        payload[key] = configuration[key]

def find_param_in_table(table, notification_type, state):
    if notification_type in table:
        return table[notification_type]
    if state in table:
        return table[state]
    if DEFAULT_STR in table:
        return table[DEFAULT_STR]
    return None

def find_param(param_type, configuration, notification_class, notification_type, state):
    if configuration is None:
        return None
    if param_type in configuration.get(notification_class, {}):
        param = find_param_in_table(configuration[notification_class][param_type],
                                    notification_type, state)
        if param is not None:
            return param
    if param_type in configuration.get(DEFAULT_STR, {}):
        param = find_param_in_table(configuration[DEFAULT_STR][param_type],
                                    notification_type, state)
        if param is not None:
            return param
    if param_type in configuration:
        return configuration[param_type]
    return None
        
parser = optparse.OptionParser()
parser.add_option('-f', '--user-configuration', help='TOML file containing configuration information.')
pushover_group = optparse.OptionGroup(parser, 'Pushover Options')
pushover_group.add_option('-T', '--token', help='Application token.  Required.')
pushover_group.add_option('-U', '--user', help='User/group key.  Required, but can be put in the configuration file.')
pushover_group.add_option('--device', help='Device to receive the notification.')
pushover_group.add_option('--priority', type='choice', choices=(-2, -1, 0, 1, 2),
                          help='Priority to use for the message.')
pushover_group.add_option('--sound', help='Sound to play for notification.')
pushover_group.add_option('--anag-instance', help='If present, the notification will contain a link to the appropriate entry in aNag (an Android Nagios client).')
parser.add_option_group(pushover_group)
nagios_group = optparse.OptionGroup(parser, 'Nagios Options')
nagios_group.add_option(      '--class', dest='notification_class')
nagios_group.add_option('-t', '--notification-type')
nagios_group.add_option('-a', '--host-alias')
nagios_group.add_option('-d', '--service-desc')
nagios_group.add_option('-w', '--time', type='int', help='Epoch seconds (use $TIMET$)')
nagios_group.add_option('-s', '--state')
nagios_group.add_option('-o', '--output')
nagios_group.add_option('-l', '--long-output')
nagios_group.add_option('-u', '--ack-author')
nagios_group.add_option('-c', '--ack-comment')
nagios_group.add_option('-n', '--notification-comment')
parser.add_option_group(nagios_group)
debug_group = optparse.OptionGroup(parser, 'Debugging Options')
debug_group.add_option(      '--debug', action='store_true')
debug_group.add_option(      '--no-send', dest='send', default=True, action='store_false', help='Don\'t actually send message to Slack.')
parser.add_option_group(debug_group)
(options, args) = parser.parse_args()

if options.token is None and options.send:
    print('No API token given.', file=sys.stderr)
    exit(1)

if options.user_configuration is None:
    configuration = None
else:
    try:
        configuration = toml.load(options.user_configuration)
    except Exception as e:
        print('Unable to read configuration file "{0}".'.format(options.user_configuration),
              file=sys.stderr)
        print(e, file=sys.stderr)
        exit(1)
            
check_option('notification type', options.notification_type)    
check_option('host alias', options.host_alias)

payload = {
    'token': options.token,
    'message': '',
}

add_param(payload, 'user', options.user, configuration)
add_param(payload, 'device', options.device, configuration)
add_param(payload, 'timestamp', options.time)

if ('user' not in payload or payload['user'] is None) and options.send:
    print('No user/group key given.', file=sys.stderr)
    exit(1)

for param in ['sound', 'priority']:
    if options.ensure_value(param, None) is not None:
        payload[param] = options.ensure_value(param, None)
    else:
        param_value = find_param(param,
                                 configuration,
                                 options.notification_class,
                                 options.notification_type,
                                 options.state)
        if param_value is not None:
            payload[param] = param_value

anag_instance = options.ensure_value('anag_instance', configuration.get('anag-instance', None))
if anag_instance is not None:
    url_params = {
        'instance': anag_instance,
        'host': options.host_alias,
        'updateonopen': 'true',
    }
    if options.service_desc is None:
        payload['url_title'] = '{0} (aNag)'.format(options.host_alias)
    else:
        url_params['service'] = options.service_desc
        payload['url_title'] = '{0} / {1} (aNag)'.format(options.host_alias, options.service_desc)
    payload['url'] = 'anag://?{0}'.format(urllib.urlencode(url_params))

## Put the notification type in the main message.
if options.notification_type in NOTIFICATION_TEXT:
    payload['title'] = NOTIFICATION_TEXT[options.notification_type]
else:
    payload['title'] = options.notification_type

if options.service_desc is not None:
    payload['title'] += ': {0} / {1}'.format(options.host_alias, options.service_desc)
else:
    payload['title'] += ': ' + options.host_alias

if options.notification_type in NOTIFICATION_STATUSES:
    payload['message'] = NOTIFICATION_STATUSES[options.notification_type]
else:
    payload['title'] += ' is {0}'.format(options.state)
    if options.output is not None:
        payload['message'] = options.output
    if options.long_output is not None and options.long_output != '':
        payload['message'] += "\n"
        payload['message'] += options.long_output

## Additional fields for acknowledgements; who ACKed and what they said.
if options.notification_type == 'ACKNOWLEDGEMENT':
    payload['message'] += "\nHandled By: {0}\nComment: {1}".format(options.ack_author, options.ack_comment)
elif options.notification_comment is not None and options.notification_comment != '':
    payload['message'] += "\nComment: {0}".format(options.notification_comment)
    
if options.debug:
    payload['message'] = "This is a test message.  Please ignore it.\n" + payload['message']

# The Pushover API requires a message.
if payload['message'] == '':
    payload['message'] = 'No output provided.'

if options.debug:
    print(json.dumps(payload))
if options.send:
    r = requests.post(PUSHOVER_API_URL, data=payload)
    if r.status_code != requests.codes.ok:
        print('Error:')
        print('\n'.join(json.loads(r.text)['errors']))
        exit(1)
