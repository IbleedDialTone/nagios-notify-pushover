# Nagios Pushover Notifications

The `notify-pushover` program here is intended to send notifications from
Nagios or similar systems (Naemon, Icinga, etc.) to [Pushover][].

  [Pushover]: https://pushover.net


## Prerequisites

You will need Python installed and the following Python modules:

 * [Requests](http://python-requests.org/)
 * [TOML](https://github.com/uiri/toml)

The program should work with Python 2 back to (at least) 2.6, and any
version of Python 3.


## Installation and Setup

Installation should be easy; just copy the `notify-pushover` script
somewhere where Nagios can run it.

### Pushover Setup

You will need to set up a Pushover application in order to use the
program.  For individuals, go to the Pushover
[Create New Application][pushover-new-app] page, fill out the fields, and
click the "Create Application" button.  From there you should be able to
get your API token.

  [pushover-new-app]: https://pushover.net/apps/build

The instructions are likely different for Pushover for Teams, but the
script author doesn't have access to that service.

Each person receiving notifications will have to provide their Pushover
user key.  That's available from the main page after they log in to the
Pushover website.

### Nagios Setup

You need to associate the Pushover user key with each contact.  One way to
do that would be like this:

    define contact {
        use  generic-contact
        
        contact_name   example
        alias          Example Admin
        _pushover_key  <user key>
        
        host_notification_commands     host-notify-pushover
        service_notification_commands  service-notify-pushover
    }

The following commands would work with the preceeding contact definition:

    define command {
        command_name host-notify-pushover
        command_line /path/to/notify-pushover \
            --token <application API token> \
            --user '$_CONTACTPUSHOVER_KEY$' \
            --notification-type '$NOTIFICATIONTYPE$' \
            --notification-comment '$NOTIFICATIONCOMMENT$' \
            --host-alias '$HOSTNAME$' \
            --state '$HOSTSTATE$' \
            --output '$HOSTOUTPUT$' \
            --long-output '$LONGHOSTOUTPUT$' \
            --ack-author '$HOSTACKAUTHOR$' \
            --ack-comment '$HOSTACKCOMMENT$' \
            --time '$TIMET$'
    }
    
    define command {
        command_name service-notify-pushover
        command_line /path/to/notify-pushover \
            --token <application API token> \
            --user '$_CONTACTPUSHOVER_KEY$' \
            --notification-type '$NOTIFICATIONTYPE$' \
            --notification-comment '$NOTIFICATIONCOMMENT$' \
            --host-alias '$HOSTNAME$' \
            --service-desc '$SERVICEDESC$' \
            --state '$SERVICESTATE$' \
            --output '$SERVICEOUTPUT$' \
            --long-output '$LONGSERVICEOUTPUT$' \
            --ack-author '$SERVICEACKAUTHOR$' \
            --ack-comment '$SERVICEACKCOMMENT$' \
            --time '$TIMET$'
    }


## Priority and Sound Settings

You can use the `--priority` and `--sound` parameters to specify the
priority and sound for the message.  See
[Message Priority][pushover-priority] and
[Notification Sounds][pushover-sounds] for the possible values and their
meanings.

  [pushover-priority]: https://pushover.net/api#priority
  [pushover-sounds]: https://pushover.net/api#sounds

You can also use the configuration file, described below, for more complex
mappings of alerts to priorities and sounds.


## Configuration File

A configuration file can be given with the `--user-configuration`
parameter.  It should be a [TOML](https://github.com/toml-lang/toml)
file.

Top-level keys (not in any tables) are as follows:

 * `user` - The user key for the person receiving the notification.
 * `device` - The device to receive the notification.
 * `anag-instance` - Same as the `--anag-instance` command line
   parameter.
 * `priority` - Default priority for notifications if no other setting
   matches.
 * `sound` - Default sound for notifications if no other setting matches.

Command line options take precedence over configuration file settings, so
if the file has a `user` key and there's a `--user` command line
parameter, the `--user` setting wins.

After the top-level keys, you may define tables to set the priorities and
sounds for various notifications.  These are chosen based on the `--class`
command line option and the `--notification-type` and `--state` options.

How you set `--class` is up to you, but it's intended to identify
different groups of hosts and services.  For example, you might have
"desktop" and "ecommerce-frontend" classes so that problems with the
ecommerce systems would generate louder, more-insistent notifications.

You can set up dotted tables for the priority and sounds for each class
like this:

    [class1.priority]
    default = -1
    DOWN = 0
    
    [class1.sound]
    PROBLEM = "falling"
    RECOVERY = "magic"
    
    [class2.priority]
    PROBLEM = 2
    RECOVERY = 1
    
    [default.sound]
    default = "bike"
    UNKNOWN = "mechanical"

Lookups work like this:

 * The program locates the table whose first dotted component matches the
   `--class` parameter.  In the examples above, that would be either
   "`class1`" or "`class2`".
 * Within each sub-table (`priority` and `sound`) that exists, the program
   first tries to match the value of the `--notification-type` parameter
   (`PROBLEM`, `RECOVERY`, `ACKNOWLEDGEMENT`, etc.).  If there's a match,
   it uses that value from the file.
 * If no match was found for the notification type, the program looks for
   a key matching the `--state` parameter (`UP`, `DOWN`, `OK`, `UNKNOWN`,
   etc.).
 * If no match was found for the state, the program looks for a key named
   "`default`" in the table.
 * If no default key was found, or if no table was found for the class,
   the program looks for a *table* named "`default`" and applies the above
   lookups (notification type, state, default) to that table.
 * If no default table was found, the program uses the top-level
   `priority` or `sound` key.
 * If those keys don't exist, the program leaves it up the the Pushover
   API's defaults.

As with other settings, command line options override the configuration
file, so if you give a `--sound` parameter, none of the above lookups will
be done for the notification sound.
